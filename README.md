![GitLabl Build Status](https://framagit.org/Obarun/66-tools/badges/master/pipeline.svg)

66-tools - Helpers tools to accomplish various and repetitive tasks in service scripts administration common tasks
====

Some utilities are language [execline](https://skarnet.org/software/execline) specific (usually named with `execl-` prefix) where other can be used on classic shell.

Installation
------------

See the INSTALL.md file.

Documentation
-------------

Online [documentation](https://web.obarun.org/software/66-tools/)

Contact information
-------------------

* Email:
  Eric Vidal `<eric@obarun.org>`

* Mailing list
  https://obarun.org/mailman/listinfo/66_obarun.org/

* Web site:
  https://web.obarun.org/

* IRC Channel:
  Network: `chat.freenode.net`
  Channel : `#obarun`

Supports the project
---------------------

Please consider to make [donation](https://web.obarun.org/index.php?id=18)
